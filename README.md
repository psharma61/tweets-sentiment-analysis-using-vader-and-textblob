# Tweets sentiment analysis using Vader and TextBlob

This project was submitted as final project for CS410-Text Information Systems Fall 2019 from University of Illinois, Urbana Champaign

# cs-410-fall2019-final-project

## CS 410 Fall 2019 Final Project - Movie Tweets Sentiment Analysis

This project is to analyze the movie tweets using Vader and to predict the user thoughts based on the tweets extract.

This Sentiment analayis project uses Jupyter notebook to share our thoughts and findings.

Please open TweetSentimentAnalysis.ipynb after installing Jupyter. We have provided the detailed explaination of each code block and explaination of end results in the notebook.

We have also added a short video ppt to demo our project and explain the results that we gathered around from this project.

Files list:

*  TweetSentimentAnalysis.ipynb - Jupyter Notebook file
*  tweets.csv - Tweets extract for #Irishman movie


Thank you.
